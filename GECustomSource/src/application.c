#include "display.h"
#include "player.h"

Application *application_create()
{
  Application *app = g_slice_new0(Application);

  if (display_create(app) && player_create(app))
  {
    return app;
  }

  application_destroy(app);
  return NULL;
}

void application_destroy(Application *app)
{
  player_destroy(app);
  display_destroy(app);

  g_slice_free(Application, app);
}

#if defined(_WIN32)
LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam)
{
  switch (message)
  {
  case WM_CREATE:
    SetWindowLongPtr(hwnd, GWLP_USERDATA, (LONG_PTR)((CREATESTRUCT *)lparam)->lpCreateParams);
    return 0;

  case WM_DESTROY:
    PostQuitMessage(0);
    return 0;

  case WM_KEYDOWN:
  {
    Application *app = (Application *)GetWindowLongPtr(hwnd, GWLP_USERDATA);
    if (app)
    {
      switch (wparam)
      {
      case VK_ESCAPE:
        application_exit(app);
        return 0;

      case VK_ADD:
        player_volume_set(app, 100);
        return 0;

      case VK_SUBTRACT:
        player_volume_set(app, 0);
        return 0;

      case VK_LEFT:
        player_seek(app, -2.5);
        return 0;

      case VK_RIGHT:
        player_seek(app, 2.5);
        return 0;

      case VK_PRIOR:
        player_seek(app, 30);
        return 0;

      case VK_NEXT:
        player_seek(app, -30);
        return 0;

      default:
        break;
      }
    }
  }
  break;

  case WM_CHAR:
  {
    Application *app = (Application *)GetWindowLongPtr(hwnd, GWLP_USERDATA);
    if (app)
    {
      switch (wparam)
      {
      case 't':
        player_toggle_state(app);
        return 0;

      case 'n':
        player_step(app, 1);
        return 0;

      case 'N':
        player_step(app, 15);
        return 0;

      case 'p':
        player_step(app, -1);
        return 0;

      case 'P':
        player_step(app, -15);
        return 0;

      default:
        break;
      }
    }
  }
  break;

  default:
    break;
  }

  return DefWindowProc(hwnd, message, wparam, lparam);
}

void application_main_loop_run(Application *app)
{
  MSG msg;

  while (!g_atomic_int_get(&app->exiting) &&
         (GetMessage(&msg, app->win, 0, 0) > 0))
  {
    TranslateMessage(&msg);
    DispatchMessage(&msg);
  }

  application_exit(app);
}

#elif defined(__linux__)
void application_main_loop_run(Application *app)
{
  XEvent event;
  KeySym key;

  while (!g_atomic_int_get(&app->exiting))
  {
    XNextEvent(app->display, &event);
    switch (event.type)
    {
    case ClientMessage:
      if (event.xclient.data.l[0] == app->delete_atom)
      {
        application_exit(app);
      }
      break;

    case KeyPress:
      key = XLookupKeysym(&event.xkey, 0);
      switch (key)
      {
      case XK_Escape:
        application_exit(app);
        break;

      case XK_t:
        player_toggle_state(app);
        break;

      case XK_n:
        if (event.xkey.state & ShiftMask)
        {
          player_step(app, 15);
        }
        else
        {
          player_step(app, 1);
        }
        break;

      case XK_p:
        if (event.xkey.state & ShiftMask)
        {
          player_step(app, -15);
        }
        else
        {
          player_step(app, -1);
        }
        break;

      case XK_KP_Add:
        player_volume_set(app, 100);
        break;

      case XK_KP_Subtract:
        player_volume_set(app, 0);
        break;

      case XK_Left:
        player_seek(app, -2.5);
        break;

      case XK_Right:
        player_seek(app, 2.5);
        break;

      case XK_Page_Up:
        player_seek(app, 30);
        break;

      case XK_Page_Down:
        player_seek(app, -30);
        break;

      default:
        break;
      }
      break;

    default:
      break;
    }
  }
}
#endif

void application_exit(Application *app)
{
  g_atomic_int_set(&app->exiting, 1);
}
