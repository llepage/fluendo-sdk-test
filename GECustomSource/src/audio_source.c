#include <stdio.h>
#include "audio_source.h"

#define DEFAULT_READ_SIZE 1024

struct _AudioSource
{
  FluSource *flu_source;
  FILE *file;
  size_t file_size;

  size_t offset;
  guchar buffer[DEFAULT_READ_SIZE];
};

static gboolean audio_source_on_need_data(FluPlayer *player, FluPlayerEvent *event, gpointer user_data)
{
  FluPlayerEventSourceNeedData *ev = (FluPlayerEventSourceNeedData *)event;
  AudioSource *audio_source = (AudioSource *)user_data;
  size_t size;

  if (ev->source != audio_source->flu_source)
  {
    return TRUE;
  }

  size = fread(audio_source->buffer, 1, DEFAULT_READ_SIZE, audio_source->file);
  if (!size || feof(audio_source->file))
  {
    if (!flu_source_custom_eos_send(audio_source->flu_source))
    {
      g_print("Error: cannot send EOS from audio source.\n");
    }

    return FALSE;
  }

  ev->pts = audio_source->offset;
  if (ev->playback_direction == FLU_PLAYBACK_DIRECTION_FORWARDS)
  {
    /* Playing forwards. */
    audio_source->offset += size;
  }
  else
  {
    /* Playing backwards. */
    if (audio_source->offset > DEFAULT_READ_SIZE)
    {
      audio_source->offset -= DEFAULT_READ_SIZE;
    }
    else
    {
      audio_source->offset = 0;
    }
    fseek(audio_source->file, audio_source->offset, SEEK_SET);
  }

  ev->size = size;
  ev->data = g_malloc(size);
  memcpy(ev->data, audio_source->buffer, size);
  ev->free_func = g_free;

  return TRUE;
}

static gboolean audio_source_on_seek(FluPlayer *player, FluPlayerEvent *event, gpointer user_data)
{
  FluPlayerEventSeekSource *ev = (FluPlayerEventSeekSource *)event;
  AudioSource *audio_source = (AudioSource *)user_data;

  if ((ev->source != audio_source->flu_source) || (ev->format != FLU_FORMAT_BYTES))
  {
    return TRUE;
  }

  audio_source->offset = ev->position;
  if (fseek(audio_source->file, audio_source->offset, SEEK_SET))
  {
    g_print("Error: cannot seek to byte offset %" SIZE_T_FORMAT " in audio stream.\n", audio_source->offset);
  }

  return TRUE;
}

AudioSource *audio_source_create(Application *app, const char *uri)
{
  FILE *file;
  long size;
  AudioSource *audio_source;
  const char *path = uri;

  if (!app->flu_player)
  {
    g_print("Error: Fluendo player not created.\n");
    return NULL;
  }

  if (!path || !path[0])
  {
    g_print("Error: invalid audio uri.\n");
    return NULL;
  }

  if (!strncmp(path, "file://", 7))
  {
    path += 7;
  }

  file = fopen(path, "rb");
  if (!file)
  {
    g_print("Error: cannot open audio uri %s.\n", uri);
    return NULL;
  }

  fseek(file, 0, SEEK_END);
  size = ftell(file);
  if (size <= 0)
  {
    fclose(file);
    g_print("Error: cannot seek audio file to the end.\n");
    return NULL;
  }
  fseek(file, 0, SEEK_SET);

  audio_source = g_slice_new0(AudioSource);
  audio_source->file = file;
  audio_source->file_size = size;
  audio_source->flu_source = flu_source_custom_new("audio", FLU_FORMAT_BYTES, size, TRUE);

  flu_player_event_listener_add(app->flu_player, FLU_PLAYER_EVENT_NEED_DATA_SOURCE, audio_source_on_need_data, audio_source);
  flu_player_event_listener_add(app->flu_player, FLU_PLAYER_EVENT_SEEK_SOURCE, audio_source_on_seek, audio_source);

  flu_player_source_add(app->flu_player, flu_source_ref(audio_source->flu_source));

  return audio_source;
}

void audio_source_destroy(Application *app, AudioSource *audio_source)
{
  if (audio_source)
  {
    if (app->flu_player)
    {
      flu_player_source_remove(app->flu_player, audio_source->flu_source);

      flu_player_event_listener_remove(app->flu_player, FLU_PLAYER_EVENT_SEEK_SOURCE, audio_source_on_seek, audio_source);
      flu_player_event_listener_remove(app->flu_player, FLU_PLAYER_EVENT_NEED_DATA_SOURCE, audio_source_on_need_data, audio_source);
    }

    flu_source_unref(audio_source->flu_source);
    fclose(audio_source->file);
    g_slice_free(AudioSource, audio_source);
  }
}
