#ifndef _VIDEO_SOURCE_H_
#define _VIDEO_SOURCE_H_

#include "application.h"

VideoSource *video_source_create(Application *app, WIN_HANDLE win, const char *video_uri);
void video_source_destroy(Application *app, VideoSource *video_source);

#endif /* _VIDEO_SOURCE_H_ */
