#include "display.h"

#if defined(_WIN32)
static LPCSTR FLUENDO_WND_CLASS_NAME = "Fluendo";
extern LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam);

gboolean display_create(Application *app)
{
  WNDCLASSEX wcx;
  RECT rect;

  if (app->instance)
  {
    g_print("Error: video window class already created.\n");
    return FALSE;
  }

  app->instance = GetModuleHandle(NULL);

  wcx.cbClsExtra = 0;
  wcx.cbSize = sizeof(WNDCLASSEX);
  wcx.cbWndExtra = 0;
  wcx.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
  wcx.hCursor = LoadCursor(NULL, IDC_ARROW);
  wcx.hIcon = LoadIcon(NULL, IDI_APPLICATION);
  wcx.hIconSm = NULL;
  wcx.hInstance = app->instance;
  wcx.lpfnWndProc = WndProc;
  wcx.lpszClassName = FLUENDO_WND_CLASS_NAME;
  wcx.lpszMenuName = 0;
  wcx.style = CS_HREDRAW | CS_VREDRAW;
  RegisterClassEx(&wcx);

  rect.left = 0;
  rect.right = 1280;
  rect.top = 0;
  rect.bottom = 720;
  AdjustWindowRectEx(&rect, WS_OVERLAPPEDWINDOW, FALSE, WS_EX_LAYERED);

  app->win = CreateWindowEx(WS_EX_LAYERED, FLUENDO_WND_CLASS_NAME,
                            TEXT("Video Window"), WS_OVERLAPPEDWINDOW,
                            CW_USEDEFAULT, CW_USEDEFAULT,
                            rect.right - rect.left, rect.bottom - rect.top,
                            NULL, NULL, app->instance, app);
  SetLayeredWindowAttributes(app->win, 0, 255, LWA_ALPHA);
  ShowWindow(app->win, SW_SHOWNORMAL);
  UpdateWindow(app->win);

  return TRUE;
}

void display_destroy(Application *app)
{
  if (app->win)
  {
    DestroyWindow(app->win);
    app->win = NULL;
  }

  if (app->instance)
  {
    UnregisterClass(FLUENDO_WND_CLASS_NAME, app->instance);
    app->instance = NULL;
  }
}

#elif defined(__linux__)
gboolean display_create(Application *app)
{
  int screen;

  if (app->display)
  {
    g_print("Error: video window already created.\n");
    return FALSE;
  }

  app->display = XOpenDisplay(NULL);
  if (!app->display)
  {
    g_print("Error: cannot open display.\n");
    return FALSE;
  }

  screen = DefaultScreen(app->display);
  app->win = XCreateSimpleWindow(app->display,
                                 RootWindow(app->display, screen),
                                 0, 0, 1280, 720, 1,
                                 BlackPixel(app->display, screen),
                                 BlackPixel(app->display, screen));
  XSelectInput(app->display, app->win, KeyPressMask);

  app->delete_atom = XInternAtom(app->display, "WM_DELETE_WINDOW", True);
  if (app->delete_atom)
  {
    XSetWMProtocols(app->display, app->win, &app->delete_atom, 1);
  }

  XMapRaised(app->display, app->win);
  XSync(app->display, FALSE);

  return TRUE;
}

void display_destroy(Application *app)
{
  if (app->display)
  {
    XCloseDisplay(app->display);
    app->display = NULL;
  }

  app->win = None;
  app->delete_atom = None;
}
#endif
