#include <stdio.h>
#include "video_source.h"

#define INDEXED_FRAMES_PER_CYCLE 64
#define FRAMES_PER_SECOND 30

/**
 * H264 video must be a bitstream of NAL units organized as:
 * - one I-Frame every second,
 * - followed by (FRAMES_PER_SECOND - 1) P-Frames,
 * - NO B-Frames.
 */
typedef struct _Frame
{
  guint64 timestamp;
  size_t offset;
  size_t size;
  gboolean is_ref_frame;
} Frame;

struct _VideoSource
{
  FluSource *flu_source;
  WIN_HANDLE win;
  FILE *file;
  size_t file_size;
  guint64 start_timestamp;

  Frame *frames;
  size_t capacity;
  size_t nb_frames;
  size_t current_frame_idx;
  gboolean seek_done;
};

static gboolean video_source_find_next_frame(VideoSource *video_source, size_t *begin, size_t *end, gboolean *is_ref_frame)
{
  int nalu_header_idx = 0;
  gboolean header_found = FALSE;
  size_t *offset = begin;

  if (!begin || !end || !is_ref_frame)
  {
    return FALSE;
  }

  for (;;)
  {
    if (header_found)
    {
      if (offset)
      {
        long position = ftell(video_source->file);
        if (position >= nalu_header_idx)
        {
          *offset = position - nalu_header_idx;
          if (offset == end)
          {
            fseek(video_source->file, -nalu_header_idx, SEEK_CUR);
            return TRUE;
          }
        }
        else
        {
          *begin = *end = 0;
          *is_ref_frame = FALSE;
          return FALSE;
        }
      }
      nalu_header_idx = 0;
    }

    int c = fgetc(video_source->file);
    if (c == EOF)
    {
      if (offset == end)
      {
        *end = video_source->file_size;
      }
      else
      {
        *begin = *end = 0;
        *is_ref_frame = FALSE;
      }
      return FALSE;
    }

    if (header_found)
    {
      header_found = FALSE;

      c &= 0x1F;
      if (c == 5) /* IDR picture */
      {
        *is_ref_frame = TRUE;
        offset = end;
      }
      else if (c == 1) /* non-IDR picture */
      {
        *is_ref_frame = FALSE;
        offset = end;
      }
      else
      {
        offset = NULL;
      }
    }
    else
    {
      switch (nalu_header_idx)
      {
      case 0:
      case 1:
        if (c == 0x00)
        {
          ++nalu_header_idx;
        }
        else
        {
          nalu_header_idx = 0;
        }
        break;

      case 2:
        if (c == 0x01)
        {
          /* Short header found: 0x00, 0x00, 0x01 */
          header_found = TRUE;
          ++nalu_header_idx;
        }
        else if (c == 0x00)
        {
          ++nalu_header_idx;
        }
        else
        {
          nalu_header_idx = 0;
        }
        break;

      case 3:
        if (c == 0x01)
        {
          /* Long header found: 0x00, 0x00, 0x00, 0x01 */
          header_found = TRUE;
          ++nalu_header_idx;
        }
        else
        {
          nalu_header_idx = 0;
        }
        break;
      }
    }
  }
}

static gboolean video_source_build_index(VideoSource *video_source)
{
  size_t begin, end;
  gboolean is_ref_frame;
  Frame *last_frame = NULL;

  if (video_source->nb_frames > 0)
  {
    last_frame = video_source->frames + (video_source->nb_frames - 1);
    if (last_frame->offset + last_frame->size >= video_source->file_size)
    {
      /* We've already reached the end of the video file. */
      return FALSE;
    }

    fseek(video_source->file, last_frame->offset + last_frame->size, SEEK_SET);
  }
  else
  {
    fseek(video_source->file, 0, SEEK_SET);
  }

  if (!video_source_find_next_frame(video_source, &begin, &end, &is_ref_frame))
  {
    /* We've reached the end of the H264 readable file.
     * Let's restrict the file size to the last readable frame position. */
    if (last_frame)
    {
      video_source->file_size = last_frame->offset + last_frame->size;
    }
    else
    {
      video_source->file_size = 0;
    }

    return FALSE;
  }

  video_source->capacity += INDEXED_FRAMES_PER_CYCLE;
  video_source->frames = g_renew(Frame, video_source->frames, video_source->capacity);
  do
  {
    Frame *frame = video_source->frames + video_source->nb_frames;
    frame->offset = begin;
    frame->size = end - begin;
    frame->is_ref_frame = is_ref_frame;
    frame->timestamp = video_source->start_timestamp + (video_source->nb_frames * TIMESTAMP_SECOND) / FRAMES_PER_SECOND;
    ++video_source->nb_frames;
  } while ((video_source->nb_frames < video_source->capacity) &&
           video_source_find_next_frame(video_source, &begin, &end, &is_ref_frame));

  return TRUE;
}

static gboolean video_source_on_request_texture(FluPlayer *player, FluPlayerEvent *event, gpointer user_data)
{
  FluPlayerEventRequestTexture *ev = (FluPlayerEventRequestTexture *)event;
  VideoSource *video_source = (VideoSource *)user_data;

  if (flu_stream_get_source(ev->stream) != video_source->flu_source)
  {
    return TRUE;
  }

  if (video_source->win)
  {
    ev->handle = (guintptr)video_source->win;
    ev->handle_aspect_ratio = TRUE;
  }

  return TRUE;
}

static gboolean video_source_on_need_data(FluPlayer *player, FluPlayerEvent *event, gpointer user_data)
{
  Frame *frame;
  size_t size;
  FluPlayerEventSourceNeedData *ev = (FluPlayerEventSourceNeedData *)event;
  VideoSource *video_source = (VideoSource *)user_data;

  if (ev->source != video_source->flu_source)
  {
    return TRUE;
  }

  /* Keep building video index if data are still available in video file. */
  video_source_build_index(video_source);

  frame = video_source->frames + video_source->current_frame_idx;
  if (ev->playback_direction == FLU_PLAYBACK_DIRECTION_FORWARDS)
  {
    /* Playing forwards.
     *
     * If video seeking has just been executed in forwards direction, we need to
     * re-send all buffers from the previous I-Frame in order to allow H264 decoder
     * to correctly rebuild current image.
     * If no seeking occurred, we can just send the current image (as any needed
     * I-Frame has already been processed by H264 decoder).
     */
    if (video_source->seek_done)
    {
      video_source->seek_done = FALSE;
      for (; !frame->is_ref_frame && (frame > video_source->frames); --frame)
      {
        --video_source->current_frame_idx;
      }
    }

    ev->pts = frame->timestamp;
    ev->size = frame->size;

    /* Advance index to next frame. */
    if (video_source->current_frame_idx < video_source->nb_frames - 1)
    {
      ++video_source->current_frame_idx;
    }
    else
    {
      g_print("Info: end of video stream reached.\n");

      if (!flu_source_custom_eos_send(video_source->flu_source))
      {
        g_print("Error: cannot send EOS from video source.\n");
      }
    }
  }
  else
  {
    /* Playing backwards. */
    video_source->seek_done = FALSE;

    ev->pts = frame->timestamp;
    ev->size = frame->size;

    /* If current frame is not an I-Frame, we need to re-send the whole block
     * from previous I-Frame until current frame (included) in order to allow
     * H264 decoder to correctly rebuild current image. */
    while (!frame->is_ref_frame && (frame > video_source->frames))
    {
      --frame;
      --video_source->current_frame_idx;
      ev->size += frame->size;
    }

    /* Move index to previous frame (which is the frame right before
     * previous I-Frame if current frame was not an I-Frame). When going
     * backwards, we need to send buffers block by block beginning by an
     * I-Frame else H264 decoder cannot rebuild current image. */
    if (video_source->current_frame_idx > 0)
    {
      --video_source->current_frame_idx;
    }
    else
    {
      g_print("Info: start of video stream reached.\n");
    }
  }

  /* Seek into video file to the right offset and copy data. */
  fseek(video_source->file, frame->offset, SEEK_SET);
  ev->data = g_malloc(ev->size);

  size = fread(ev->data, 1, ev->size, video_source->file);
  if ((size != ev->size) || feof(video_source->file))
  {
    g_free(ev->data);
    ev->data = NULL;
    ev->size = 0;

    g_print("Error: cannot read data from video stream.\n");

    if (!flu_source_custom_eos_send(video_source->flu_source))
    {
      g_print("Error: cannot send EOS from video source.\n");
    }

    return FALSE;
  }

  ev->free_func = g_free;
  return TRUE;
}

static gboolean video_source_on_seek(FluPlayer *player, FluPlayerEvent *event, gpointer user_data)
{
  FluPlayerEventSeekSource *ev = (FluPlayerEventSeekSource *)event;
  VideoSource *video_source = (VideoSource *)user_data;
  int low_idx, high_idx, mid_idx;

  if ((ev->source != video_source->flu_source) || (ev->format != FLU_FORMAT_TIME))
  {
    return TRUE;
  }

  low_idx = 0;
  high_idx = video_source->nb_frames;
  while (low_idx < high_idx)
  {
    mid_idx = (low_idx + high_idx) >> 1;
    if (ev->position > video_source->frames[mid_idx].timestamp)
    {
      low_idx = mid_idx + 1;
    }
    else
    {
      high_idx = mid_idx;
    }
  }

  video_source->current_frame_idx = (low_idx > 0) ? low_idx - 1 : 0;
  video_source->seek_done = TRUE;

  return TRUE;
}

VideoSource *video_source_create(Application *app, WIN_HANDLE win, const char *video_uri)
{
  FILE *file;
  long size;
  VideoSource *video_source;
  const char *path = video_uri;

  if (!app->flu_player)
  {
    g_print("Error: Fluendo player not created.\n");
    return NULL;
  }

  if (!path || !path[0])
  {
    g_print("Error: invalid video uri.\n");
    return NULL;
  }

  if (!strncmp(path, "file://", 7))
  {
    path += 7;
  }

  file = fopen(path, "rb");
  if (!file)
  {
    g_print("Error: cannot open video uri %s.\n", video_uri);
    return NULL;
  }

  fseek(file, 0, SEEK_END);
  size = ftell(file);
  if (size <= 0)
  {
    fclose(file);
    g_print("Error: cannot seek video file to the end.\n");
    return NULL;
  }

  video_source = g_slice_new0(VideoSource);
  video_source->win = win;
  video_source->file = file;
  video_source->file_size = size;
  if (!video_source_build_index(video_source))
  {
    fclose(video_source->file);
    g_slice_free(VideoSource, video_source);
    return NULL;
  }
  video_source->flu_source = flu_source_custom_new("video", FLU_FORMAT_TIME, FLU_SIZE_UNKNOWN, TRUE);

  flu_player_event_listener_add(app->flu_player, FLU_PLAYER_EVENT_REQUEST_TEXTURE, video_source_on_request_texture, video_source);
  flu_player_event_listener_add(app->flu_player, FLU_PLAYER_EVENT_NEED_DATA_SOURCE, video_source_on_need_data, video_source);
  flu_player_event_listener_add(app->flu_player, FLU_PLAYER_EVENT_SEEK_SOURCE, video_source_on_seek, video_source);

  flu_player_source_add(app->flu_player, flu_source_ref(video_source->flu_source));

  return video_source;
}

void video_source_destroy(Application *app, VideoSource *video_source)
{
  if (video_source)
  {
    if (app->flu_player)
    {
      flu_player_source_remove(app->flu_player, video_source->flu_source);

      flu_player_event_listener_remove(app->flu_player, FLU_PLAYER_EVENT_SEEK_SOURCE, video_source_on_seek, video_source);
      flu_player_event_listener_remove(app->flu_player, FLU_PLAYER_EVENT_NEED_DATA_SOURCE, video_source_on_need_data, video_source);
    }

    flu_source_unref(video_source->flu_source);
    fclose(video_source->file);
    g_free(video_source->frames);
    g_slice_free(VideoSource, video_source);
  }
}
