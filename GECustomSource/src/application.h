#ifndef _APPLICATION_H_
#define _APPLICATION_H_

#include <fluendo-sdk.h>

#if defined(_WIN32)
#include <windows.h>
#define SIZE_T_FORMAT "Iu"
#define WIN_HANDLE HWND
#elif defined(__linux__)
#include <X11/Xlib.h>
#include <X11/keysym.h>
#define SIZE_T_FORMAT "zu"
#define WIN_HANDLE Window
#endif

#define TIMESTAMP_SECOND 1000000000L

typedef struct _AudioSource AudioSource;
typedef struct _VideoSource VideoSource;

typedef struct _Application
{
  FluPlayer *flu_player;
  GList *audio_sources;
  GList *video_sources;
  volatile gint exiting;
  WIN_HANDLE win;

#if defined(_WIN32)
  HINSTANCE instance;
#elif defined(__linux__)
  Display *display;
  Atom delete_atom;
#endif
} Application;

Application *application_create();
void application_destroy(Application *app);

void application_main_loop_run(Application *app);
void application_exit(Application *app);

#endif /* _APPLICATION_H_ */
