#include "player.h"
#include "video_source.h"
#include "audio_source.h"

static gboolean player_on_error(FluPlayer *player, FluPlayerEvent *event, gpointer user_data)
{
  FluPlayerEventError *ev = (FluPlayerEventError *)event;
  Application *app = (Application *)user_data;

  g_print("Error: %s <%s>.\n", ev->error->message, ev->dbg);
  flu_player_dot_graph_generate(player, "player_error");

  application_exit(app);
  return FALSE;
}

static gboolean player_on_eos(FluPlayer *player, FluPlayerEvent *event, gpointer user_data)
{
  Application *app = (Application *)user_data;

  g_print("Info: EOS received.\n");

  application_exit(app);
  return FALSE;
}

static gboolean player_on_state(FluPlayer *player, FluPlayerEvent *event, gpointer user_data)
{
  const char *state;
  FluPlayerEventState *ev = (FluPlayerEventState *)event;

  if (ev->state == FLU_PLAYER_STATE_PLAYING)
  {
    flu_player_dot_graph_generate(player, "player_start_playing");
  }

  switch (ev->state)
  {
  case FLU_PLAYER_STATE_SEEKING:
    state = "FLU_PLAYER_STATE_SEEKING";
    break;

  case FLU_PLAYER_STATE_PAUSED:
    state = "FLU_PLAYER_STATE_PAUSED";
    break;

  case FLU_PLAYER_STATE_PLAYING:
    state = "FLU_PLAYER_STATE_PLAYING";
    break;

  default:
    state = "FLU_PLAYER_STATE_STOPPED";
    break;
  }

  g_print("Info: player state %s.\n", state);
  return TRUE;
}

gboolean player_create(Application *app)
{
  if (app->flu_player)
  {
    g_print("Error: Fluendo player already created.\n");
    return FALSE;
  }

  if (!app->win)
  {
    g_print("Error: video window not created.\n");
    return FALSE;
  }

  flu_initialize();
  app->flu_player = flu_player_new();
  flu_player_hw_accel_set(app->flu_player, FALSE);

  flu_player_event_listener_add(app->flu_player, FLU_PLAYER_EVENT_ERROR, player_on_error, app);
  flu_player_event_listener_add(app->flu_player, FLU_PLAYER_EVENT_EOS, player_on_eos, app);
  flu_player_event_listener_add(app->flu_player, FLU_PLAYER_EVENT_STATE, player_on_state, app);

  return TRUE;
}

static void player_destroy_all_sources(Application *app)
{
  GList *l;

  for (l = app->video_sources; l; l = g_list_next(l))
  {
    video_source_destroy(app, l->data);
  }
  g_list_free(app->video_sources);
  app->video_sources = NULL;

  for (l = app->audio_sources; l; l = g_list_next(l))
  {
    audio_source_destroy(app, l->data);
  }
  g_list_free(app->audio_sources);
  app->audio_sources = NULL;
}

void player_destroy(Application *app)
{
  if (app->flu_player)
  {
    flu_player_close(app->flu_player);
    player_destroy_all_sources(app);

    flu_player_unref(app->flu_player);
    app->flu_player = NULL;

    flu_shutdown();
  }
}

void player_add_video_source(Application *app, const char *video_uri)
{
  if (app->flu_player)
  {
    VideoSource *video_source = video_source_create(app, app->win, video_uri);
    if (video_source)
    {
      app->video_sources = g_list_append(app->video_sources, video_source);
    }
  }
}

void player_add_audio_source(Application *app, const char *audio_uri)
{
  if (app->flu_player)
  {
    AudioSource *audio_source = audio_source_create(app, audio_uri);
    if (audio_source)
    {
      app->audio_sources = g_list_append(app->audio_sources, audio_source);
    }
  }
}

void player_toggle_state(Application *app)
{
  if (app->flu_player)
  {
    g_print("Info: toggling player state.\n");

    switch (flu_player_state_get(app->flu_player))
    {
    case FLU_PLAYER_STATE_PAUSED:
    case FLU_PLAYER_STATE_STOPPED:
      flu_player_play(app->flu_player);
      break;

    case FLU_PLAYER_STATE_PLAYING:
      flu_player_pause(app->flu_player);
      break;

    default:
      break;
    }
  }
}

void player_seek(Application *app, gfloat time_offset)
{
  if (app->flu_player)
  {
    gint64 position;
    g_print("Info: seeking %.2f seconds from now.\n", time_offset);

    if (!flu_player_position_get(app->flu_player, &position))
    {
      g_print("Error: cannot get current player position.\n");
      return;
    }

    position += (gint64)(TIMESTAMP_SECOND * time_offset);
    if (position < 0)
    {
      position = 0;
    }

    if (!flu_player_position_set(app->flu_player, position, FALSE))
    {
      g_print("Error: cannot set player position.\n");
    }
  }
}

void player_volume_set(Application *app, gint volume)
{
  if (app->flu_player)
  {
    GList *item;
    gfloat fvol = CLAMP((gfloat)volume / 100.f, 0.f, 1.f);
    GList *streams = flu_player_audio_active_streams_get(app->flu_player);

    for (item = streams; item; item = item->next)
    {
      FluStream *stream = item->data;
      flu_stream_volume_set(stream, fvol);
    }

    flu_stream_list_free(streams);
  }
}

void player_step(Application *app, gint64 amount)
{
  if (app->flu_player)
  {
    flu_player_pause(app->flu_player);

    if (!flu_player_step(app->flu_player, amount))
    {
      g_print("Error: player cannot steps by %" G_GINT64_FORMAT " buffers.\n", amount);
    }
  }
}
