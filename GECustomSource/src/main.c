#include "player.h"

int main(int argc, char **argv)
{
  if (argc < 3)
  {
    g_print("Usage: %s <H264> <WAV>\n"
            "H264: Input video file in raw H264 bitstream with:\n"
            "      - one I-Frame every second,\n"
            "      - 29 P-Frames after each I-Frame,\n"
            "      - no B-Frames.\n"
            "WAV: Input audio wav file in u-law 8 kHz bitstream.\n",
            argv[0]);
    return 1;
  }

  Application *app = application_create();
  if (!app)
  {
    return 2;
  }

  player_add_video_source(app, argv[1]);
  player_add_audio_source(app, argv[2]);
  player_toggle_state(app);

  application_main_loop_run(app);
  application_destroy(app);

  return 0;
}

#ifdef _WIN32
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
  return main(__argc, __argv);
}
#endif
