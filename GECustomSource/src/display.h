#ifndef _DISPLAY_H_
#define _DISPLAY_H_

#include "application.h"

gboolean display_create(Application *app);
void display_destroy(Application *app);

#endif /* _DISPLAY_H_ */
