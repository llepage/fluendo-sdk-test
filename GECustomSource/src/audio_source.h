#ifndef _AUDIO_SOURCE_H_
#define _AUDIO_SOURCE_H_

#include "application.h"

AudioSource *audio_source_create(Application *app, const char *uri);
void audio_source_destroy(Application *app, AudioSource *audio_source);

#endif /* _AUDIO_SOURCE_H_ */
