#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "application.h"

gboolean player_create(Application *app);
void player_destroy(Application *app);

void player_add_video_source(Application *app, const char *video_uri);
void player_add_audio_source(Application *app, const char *audio_uri);

void player_toggle_state(Application *app);
void player_seek(Application *app, gfloat time_offset);
void player_volume_set(Application *app, gint volume);
void player_step(Application *app, gint64 amount);

#endif /* _PLAYER_H_ */
