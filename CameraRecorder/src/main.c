#include <fluendo-sdk.h>
#include <stdio.h>

static gboolean _exiting = FALSE;

static gboolean _handle_keyboard(GIOChannel *source, GIOCondition cond, gpointer data)
{
  gchar *str = NULL;
  GMainLoop *main_loop = (GMainLoop *)data;

  if (g_io_channel_read_line(source, &str, NULL, NULL, NULL) == G_IO_STATUS_NORMAL)
  {

    if ((str[0] == 'q') && !_exiting)
    {
      _exiting = TRUE;
      g_print("Exiting...\n");
      g_main_loop_quit(main_loop);
    }

    g_free(str);
  }

  return TRUE;
}

static gboolean _on_request_save_mode(FluRecorder *recorder, FluRecorderEvent *event, gpointer data)
{
  FluRecorderEventRequestSaveMode *ev = (FluRecorderEventRequestSaveMode *)event;

  ev->mode = FLU_RECORDER_SAVE_MODE_FILE;
  ev->data.file.filename = "./720.mp4";

  return TRUE;
}

static gboolean _on_state_changed(FluRecorder *recorder, FluRecorderEvent *event, gpointer data)
{
  FluRecorderEventState *ev = (FluRecorderEventState *)event;

  g_print("State changed to %s\n", flu_recorder_state_name_get(ev->state));

  return TRUE;
}

static FluDevice *_find_fake_camera()
{
  GList *devices, *tmp;
  FluDevice *fake_camera = NULL;

  devices = flu_device_list_get();
  for (tmp = devices; tmp; tmp = tmp->next)
  {
    FluDevice *device = (FluDevice *)tmp->data;
    if ((flu_device_type_get(device) == FLU_DEVICE_TYPE_CAMERA) && !strcmp(flu_device_current_media_id_get(device), "videotestsrc"))
    {
      fake_camera = flu_device_ref(device);
      break;
    }
  }

  flu_device_list_free(devices);
  return fake_camera;
}

int main(int argc, char **argv)
{
  GMainLoop *main_loop;
  FluMediaInfo media_info;
  FluRecorder *recorder;
  FluDevice *fake_camera;
  FluStreamInfo stream_info;
  GIOChannel *io_stdin;

  flu_initialize();
  main_loop = g_main_loop_new(NULL, FALSE);

  memset(&media_info, 0, sizeof(media_info));
  media_info.format = FLU_MEDIA_INFO_FORMAT_MP4;
  recorder = flu_recorder_new(&media_info);

  flu_recorder_event_listener_add(recorder, FLU_RECORDER_EVENT_REQUEST_SAVE_MODE, _on_request_save_mode, NULL);
  flu_recorder_event_listener_add(recorder, FLU_RECORDER_EVENT_STATE, _on_state_changed, NULL);

  fake_camera = _find_fake_camera();
  memset(&stream_info, 0, sizeof(stream_info));
  stream_info.type = FLU_STREAM_TYPE_VIDEO;
  stream_info.data.video.width = 1280;
  stream_info.data.video.height = 720;
  stream_info.data.video.bitrate = 2000000;
  stream_info.data.video.fps_n = 25;
  stream_info.data.video.fps_d = 1;
  stream_info.data.video.vcodec = FLU_STREAM_VIDEO_CODEC_H264;
  flu_recorder_connect_device(recorder, fake_camera, &stream_info);

  io_stdin = g_io_channel_unix_new(fileno(stdin));
  g_io_add_watch(io_stdin, G_IO_IN, (GIOFunc)_handle_keyboard, main_loop);

  g_print("Recording started. Press q<Enter> to exit.\n");
  flu_recorder_record(recorder);
  flu_recorder_dot_graph_generate(recorder, "recoder_graph");
  g_main_loop_run(main_loop);

  flu_recorder_stop(recorder);
  flu_recorder_unref(recorder);
  flu_device_unref(fake_camera);

  flu_shutdown();
  g_main_loop_unref(main_loop);

  return 0;
}
