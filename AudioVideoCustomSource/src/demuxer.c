#include "demuxer.h"
#include "player.h"

static GstCaps *get_pad_caps(GstPad *pad)
{
  GstStructure *s;
  const gchar *name;

  GstCaps *caps = gst_pad_get_caps_reffed(pad);
  if (gst_caps_get_size(caps) <= 0)
  {
    gst_object_unref(caps);
    return NULL;
  }

  s = gst_caps_get_structure(caps, 0);
  if (!s)
  {
    gst_object_unref(caps);
    return NULL;
  }

  name = gst_structure_get_name(s);
  if (g_str_has_prefix(name, "video") ||
      g_str_has_prefix(name, "audio"))
  {
    return caps;
  }

  gst_object_unref(caps);
  g_print("Error: pad with caps %s is not supported.\n", name);

  return NULL;
}

static void on_pad_added(GstElement *src, GstPad *pad, Application *app)
{
  GstElement *parser = NULL;
  GstElement *filter = NULL;
  GstElement *demuxer_sink = NULL;
  GstPad *sinkpad = NULL;
  GstStructure *s = NULL;
  CustomSourceType source_type = UNKNOWN_SOURCE;

  GstCaps *caps = get_pad_caps(pad);
  if (!caps)
  {
    return;
  }

  demuxer_sink = gst_element_factory_make("appsink", NULL);

  s = gst_caps_get_structure(caps, 0);
  if (gst_structure_has_name(s, "video/x-h264"))
  {
    source_type = VIDEO_SOURCE;

    parser = gst_element_factory_make("h264parse", NULL);
    filter = gst_element_factory_make("capsfilter", NULL);
    gst_structure_set(s,
                      "stream-format", G_TYPE_STRING, "byte-stream",
                      "alignment", G_TYPE_STRING, "nal",
                      NULL);
    g_object_set(G_OBJECT(filter), "caps", caps, NULL);

    gst_bin_add_many(GST_BIN(app->demuxer_pipeline), parser, filter, demuxer_sink, NULL);
    gst_element_link_many(parser, filter, demuxer_sink, NULL);

    sinkpad = gst_element_get_static_pad(parser, "sink");
  }
  else if (gst_structure_has_name(s, "audio/mpeg"))
  {
    gint mpeg_version = 0;
    gst_structure_get_int(s, "mpegversion", &mpeg_version);

    source_type = AUDIO_SOURCE;
    if (mpeg_version == 1)
    {
      parser = gst_element_factory_make("mpegaudioparse", NULL);

      gst_bin_add_many(GST_BIN(app->demuxer_pipeline), parser, demuxer_sink, NULL);
      gst_element_link_many(parser, demuxer_sink, NULL);

      sinkpad = gst_element_get_static_pad(parser, "sink");
    }
    else
    {
      gst_bin_add_many(GST_BIN(app->demuxer_pipeline), demuxer_sink, NULL);
      sinkpad = gst_element_get_static_pad(demuxer_sink, "sink");
    }
  }
  else
  {
    gst_bin_add_many(GST_BIN(app->demuxer_pipeline), demuxer_sink, NULL);
    sinkpad = gst_element_get_static_pad(demuxer_sink, "sink");
  }

  if (parser)
  {
    if (G_UNLIKELY(!gst_element_sync_state_with_parent(parser)))
    {
      GST_WARNING("failed synchronizing parser state with parent");
    }
  }

  if (filter)
  {
    if (G_UNLIKELY(!gst_element_sync_state_with_parent(filter)))
    {
      GST_WARNING("failed synchronizing filter state with parent");
    }
  }

  if (demuxer_sink)
  {
    if (G_UNLIKELY(!gst_element_sync_state_with_parent(demuxer_sink)))
    {
      GST_WARNING("failed synchronizing demuxer sink state with parent");
    }
  }

  if (G_LIKELY(sinkpad))
  {
    if (gst_pad_link(pad, sinkpad) == GST_PAD_LINK_OK)
    {
      if (!player_create_custom_source(app, demuxer_sink, source_type))
      {
        GST_WARNING("failed to create custom player source");
      }
    }
    else
    {
      GST_WARNING("failed linking src pad with sink");
    }

    gst_object_unref(sinkpad);
  }

  GST_DEBUG_BIN_TO_DOT_FILE(GST_BIN(app->demuxer_pipeline), GST_DEBUG_GRAPH_SHOW_ALL, "demuxer_pipeline");
  gst_caps_unref(caps);
}

static void on_no_more_pads(GstElement *src, Application *app)
{
  g_print("Info: all sources demuxed.\n");
  player_toggle_state(app);
}

static GstAutoplugSelectResult on_autoplug_select(GstElement *src, GstPad *pad, GstCaps *caps, GstElementFactory *factory, Application *app)
{
  GstAutoplugSelectResult ret = GST_AUTOPLUG_SELECT_TRY;
  if (gst_element_factory_list_is_type(factory,
                                       GST_ELEMENT_FACTORY_TYPE_PARSER |
                                           GST_ELEMENT_FACTORY_TYPE_DEPAYLOADER |
                                           GST_ELEMENT_FACTORY_TYPE_DECODER |
                                           GST_ELEMENT_FACTORY_TYPE_SINK))
  {
    g_print("Info: exposing demuxer pad for %s.\n", gst_element_factory_get_longname(factory));
    ret = GST_AUTOPLUG_SELECT_EXPOSE;
  }

  return ret;
}

gboolean demuxer_open_uri(Application *app, const char *uri)
{
  GstElement *demuxer_src;

  if (app->demuxer_pipeline)
  {
    return FALSE;
  }

  app->demuxer_pipeline = (GstPipeline *)gst_pipeline_new(NULL);
  demuxer_src = gst_element_factory_make("uridecodebin", NULL);

  gst_bin_add(GST_BIN(app->demuxer_pipeline), demuxer_src);
  g_object_set(G_OBJECT(demuxer_src), "uri", uri, NULL);

  g_signal_connect(G_OBJECT(demuxer_src), "pad-added", G_CALLBACK(on_pad_added), app);
  g_signal_connect(G_OBJECT(demuxer_src), "no-more-pads", G_CALLBACK(on_no_more_pads), app);
  g_signal_connect(G_OBJECT(demuxer_src), "autoplug-select", G_CALLBACK(on_autoplug_select), app);

  gst_element_set_state(GST_ELEMENT(app->demuxer_pipeline), GST_STATE_PLAYING);

  return TRUE;
}

void demuxer_destroy(Application *app)
{
  if (app->demuxer_pipeline)
  {
    gst_element_set_state(GST_ELEMENT(app->demuxer_pipeline), GST_STATE_NULL);
    gst_object_unref(app->demuxer_pipeline);
    app->demuxer_pipeline = NULL;
  }
}

void demuxer_seek(CustomSource *source, FluFormat format, guint64 position)
{
  if (source->type == VIDEO_SOURCE)
  {
    GstElement *demuxer_pipeline = GST_ELEMENT(source->app->demuxer_pipeline);
    if (demuxer_pipeline)
    {
      GstState state;
      if ((gst_element_get_state(demuxer_pipeline, &state, NULL, 0) == GST_STATE_CHANGE_SUCCESS) &&
          (state == GST_STATE_PLAYING))
      {
        if (!gst_element_seek_simple(demuxer_pipeline,
                                     (format == FLU_FORMAT_BYTES) ? GST_FORMAT_BYTES : GST_FORMAT_TIME,
                                     GST_SEEK_FLAG_FLUSH | GST_SEEK_FLAG_KEY_UNIT,
                                     position))
        {
          g_print("Error: cannot seek on demuxer stream.\n");
        }
      }
    }
  }
}
