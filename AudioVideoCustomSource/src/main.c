#include "demuxer.h"
#include "player.h"

static gboolean handle_keyboard(GIOChannel *channel, GIOCondition cond, gpointer user_data)
{
  Application *app = (Application *)user_data;
  gchar *str = NULL;

  if (g_io_channel_read_line(channel, &str, NULL, NULL, NULL) == G_IO_STATUS_NORMAL)
  {
    const gchar *p = str;

    while ((*p < 'a') || (*p > 'z'))
    {
      ++p;
    }

    switch (*p)
    {
    case 't':
      player_toggle_state(app);
      break;

    case 's':
      player_seek(app, atoi(++p));
      break;

    case 'p':
      player_print_position(app);
      break;

    case 'q':
      application_exit(app);
      break;

    case 'v':
      player_volume_set(app, atoi(++p));
      break;

    case 'f':
      {
        gint amount = atoi(++p);
        player_step(app, MAX(amount, 0));
      }
      break;
    }
  }

  g_free(str);
  return TRUE;
}

int main(int argc, char **argv)
{
  GIOChannel *io_stdin;
  Application *app;

  if (argc < 2)
  {
    g_print("Usage: %s <input file>\n"
            "Input file uri with H264 video and MP3 audio.\n",
            argv[0]);
    return 1;
  }

  /* Init application */
  flu_initialize();

  app = g_slice_new0(Application);
  demuxer_open_uri(app, argv[1]);

  /* Add a keyboard watch for user input */
#ifdef _WIN32
  io_stdin = g_io_channel_unix_new(_fileno(stdin));
#else
  io_stdin = g_io_channel_unix_new(fileno(stdin));
#endif
  g_io_add_watch(io_stdin, G_IO_IN, (GIOFunc)handle_keyboard, app);

  /* Main loop */
  app->main_loop = g_main_loop_new(NULL, FALSE);
  g_main_loop_run(app->main_loop);

  /* Free resources */
  g_main_loop_unref(app->main_loop);
  app->main_loop = NULL;

  player_destroy(app);
  demuxer_destroy(app);
  g_slice_free(Application, app);

  flu_shutdown();
  return 0;
}
