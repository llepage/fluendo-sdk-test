#include "application.h"

void application_exit(Application *app)
{
  g_print("Exiting...\n");
  g_main_loop_quit(app->main_loop);
}
