#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "application.h"

gboolean player_create_custom_source(Application *app, GstElement *demuxer_sink, CustomSourceType source_type);
void player_destroy(Application *app);

void player_toggle_state(Application *app);
void player_seek(Application *app, gint time_offset);
void player_print_position(Application *app);
void player_volume_set(Application *app, gint volume);
void player_step(Application *app, guint64 amount);

#endif /* _PLAYER_H_ */
