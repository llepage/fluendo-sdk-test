#ifndef _GST_COMPAT_H_
#define _GST_COMPAT_H_

#include <gst/gst.h>

typedef struct
{
  guint8 *data;
  gsize size;
} GstMapInfo;

typedef enum
{
  GST_MAP_READ = 1 << 0,
  GST_MAP_WRITE = 1 << 1
} GstMapFlags;

static inline gboolean gst_buffer_map(GstBuffer *buffer, GstMapInfo *info, GstMapFlags flags)
{
  info->data = GST_BUFFER_DATA(buffer);
  info->size = GST_BUFFER_SIZE(buffer);
  return TRUE;
}

#define gst_buffer_unmap(buffer, info) while (0)

typedef enum
{
  GST_AUTOPLUG_SELECT_TRY,
  GST_AUTOPLUG_SELECT_EXPOSE,
  GST_AUTOPLUG_SELECT_SKIP
} GstAutoplugSelectResult;

#endif /* _GST_COMPAT_H_ */
