#ifndef _APPLICATION_H_
#define _APPLICATION_H_

#include <fluendo-sdk.h>
#include "gst-compat.h"

#if defined(_WIN32)
#include <windows.h>
#elif defined(__linux__)
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#endif

typedef struct
{
  FluPlayer *flu_player;
  GstPipeline *demuxer_pipeline;
  GMainLoop *main_loop;
  GList *custom_sources;
} Application;

typedef enum
{
  UNKNOWN_SOURCE,
  VIDEO_SOURCE,
  AUDIO_SOURCE
} CustomSourceType;

typedef struct
{
  Application *app;
  CustomSourceType type;

  GstElement *demuxer_sink;
  FluSource *flu_source;

  int width;
  int height;

#if defined(_WIN32)
  HWND hWnd;
  HANDLE window_created_signal;
#elif defined(__linux__)
  Display *display;
#endif
} CustomSource;

void application_exit(Application *app);

#endif /* _APPLICATION_H_ */
