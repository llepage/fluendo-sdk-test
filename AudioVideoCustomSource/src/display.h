#ifndef _DISPLAY_H_
#define _DISPLAY_H_

#include "application.h"

gboolean display_create(FluPlayerEventRequestTexture *event, CustomSource *source);
void display_close(CustomSource *source);

#endif /* _DISPLAY_H_ */
