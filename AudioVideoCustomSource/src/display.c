#include "display.h"

#if defined(_WIN32)

static LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam)
{
  switch (message)
  {
  case WM_DESTROY:
    PostQuitMessage(0);
    return 0;

  default:
    return DefWindowProc(hwnd, message, wparam, lparam);
  }
}

static gpointer window_thread(CustomSource *source)
{
  WNDCLASSEX wcx;
  RECT rect;
  MSG msg;
  HINSTANCE hInstance = GetModuleHandle(NULL);
  LPCSTR class_name = "Fluendo";

  wcx.cbClsExtra = 0;
  wcx.cbSize = sizeof(WNDCLASSEX);
  wcx.cbWndExtra = 0;
  wcx.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
  wcx.hCursor = LoadCursor(NULL, IDC_ARROW);
  wcx.hIcon = LoadIcon(NULL, IDI_APPLICATION);
  wcx.hIconSm = NULL;
  wcx.hInstance = hInstance;
  wcx.lpfnWndProc = WndProc;
  wcx.lpszClassName = class_name;
  wcx.lpszMenuName = 0;
  wcx.style = CS_HREDRAW | CS_VREDRAW;
  RegisterClassEx(&wcx);

  rect.left = 0;
  rect.right = source->width;
  rect.top = 0;
  rect.bottom = source->height;
  AdjustWindowRectEx(&rect, WS_OVERLAPPEDWINDOW, FALSE, WS_EX_LAYERED);

  source->hWnd = CreateWindowEx(WS_EX_LAYERED, class_name,
                                TEXT("Video Window"), WS_OVERLAPPEDWINDOW,
                                CW_USEDEFAULT, CW_USEDEFAULT,
                                rect.right - rect.left, rect.bottom - rect.top,
                                NULL, NULL, hInstance, source);
  SetLayeredWindowAttributes(source->hWnd, 0, 255, LWA_ALPHA);

  ShowWindow(source->hWnd, SW_SHOWNORMAL);
  UpdateWindow(source->hWnd);
  ReleaseSemaphore(source->window_created_signal, 1, NULL);

  /* Window message loop */
  while (GetMessage(&msg, source->hWnd, 0, 0) > 0)
  {
    TranslateMessage(&msg);
    DispatchMessage(&msg);
  }

  g_print("Video window has been closed.\n");
  DestroyWindow(source->hWnd);
  UnregisterClass(class_name, hInstance);

  return NULL;
}

gboolean display_create(FluPlayerEventRequestTexture *event, CustomSource *source)
{
  const FluStreamInfo *info;

  if (source->hWnd)
  {
    g_print("Error: video window already created.\n");
    return FALSE;
  }

  if (!flu_stream_info_get(event->stream, &info))
  {
    g_print("Error: cannot get video stream info.\n");
    return FALSE;
  }

  g_print("Info: video size is %d x %d.\n", info->data.video.width, info->data.video.height);
  source->width = info->data.video.width;
  source->height = info->data.video.height;

  source->window_created_signal = CreateSemaphore(NULL, 0, 1, NULL);
  if (!source->window_created_signal)
  {
    g_print("Error: cannot create window creation signal.\n");
    return FALSE;
  }

  g_thread_new("window_thread", (GThreadFunc)window_thread, source);
  if (WaitForSingleObject(source->window_created_signal, INFINITE) != WAIT_OBJECT_0)
  {
    CloseHandle(source->window_created_signal);
    g_print("Error: cannot create video window.\n");
    return FALSE;
  }
  CloseHandle(source->window_created_signal);

  if (!source->hWnd)
  {
    g_print("Error: cannot create video window.\n");
    return FALSE;
  }

  event->handle = (guintptr)source->hWnd;
  event->handle_aspect_ratio = TRUE;

  return TRUE;
}

void display_close(CustomSource *source)
{
  if (source->hWnd)
  {
    PostMessage(source->hWnd, WM_DESTROY, 0, 0);
  }
}

#elif defined(__linux__)

gboolean display_create(FluPlayerEventRequestTexture *event, CustomSource *source)
{
  const FluStreamInfo *info;
  int screen;
  Window win;
  XSizeHints hints;
  Atom delete_atom;

  if (source->display)
  {
    g_print("Error: video window already created.\n");
    return FALSE;
  }

  if (!flu_stream_info_get(event->stream, &info))
  {
    g_print("Error: cannot get video stream info.\n");
    return FALSE;
  }

  g_print("Info: video size is %d x %d.\n", info->data.video.width, info->data.video.height);
  source->width = info->data.video.width;
  source->height = info->data.video.height;

  source->display = XOpenDisplay(NULL);
  if (!source->display)
  {
    g_print("Error: cannot open display.\n");
    return FALSE;
  }

  screen = DefaultScreen(source->display);
  win = XCreateSimpleWindow(source->display,
                            RootWindow(source->display, screen),
                            0, 0, source->width, source->height, 1,
                            BlackPixel(source->display, screen),
                            WhitePixel(source->display, screen));
  XSetWindowBackgroundPixmap(source->display, win, None);

  hints.flags = PMinSize | PMaxSize | USSize;
  hints.width = hints.min_width = hints.max_width = source->width;
  hints.height = hints.min_height = hints.max_height = source->height;
  XSetWMNormalHints(source->display, win, &hints);

  delete_atom = XInternAtom(source->display, "WM_DELETE_WINDOW", True);
  if (delete_atom)
  {
    XSetWMProtocols(source->display, win, &delete_atom, 1);
  }

  XMapRaised(source->display, win);
  XSync(source->display, FALSE);

  event->handle = win;
  event->handle_aspect_ratio = TRUE;

  return TRUE;
}

void display_close(CustomSource *source)
{
  if (source->display)
  {
    XCloseDisplay(source->display);
    source->display = NULL;
  }
}

#else /* !_WIN32 && !__linux__ */

gboolean display_create(FluPlayerEventRequestTexture *event, CustomSource *source)
{
  g_print("Error: video window not implemented on your platform.\n");
  return TRUE;
}

void display_close(CustomSource *source)
{
}

#endif
