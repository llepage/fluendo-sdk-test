#ifndef _DEMUXER_H_
#define _DEMUXER_H_

#include "application.h"

gboolean demuxer_open_uri(Application *app, const char *uri);
void demuxer_destroy(Application *app);

void demuxer_seek(CustomSource *source, FluFormat format, guint64 position);

#endif /* _DEMUXER_H_ */
