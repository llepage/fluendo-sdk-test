#include "player.h"
#include "display.h"
#include "demuxer.h"

static gboolean on_state(FluPlayer *player, FluPlayerEvent *event, gpointer user_data)
{
  const char *state;
  FluPlayerEventState *ev = (FluPlayerEventState *)event;

  if (ev->state == FLU_PLAYER_STATE_PLAYING)
  {
    flu_player_dot_graph_generate(player, "custom_source_playing");
  }

  switch (ev->state)
  {
  case FLU_PLAYER_STATE_SEEKING:
    state = "FLU_PLAYER_STATE_SEEKING";
    break;

  case FLU_PLAYER_STATE_PAUSED:
    state = "FLU_PLAYER_STATE_PAUSED";
    break;

  case FLU_PLAYER_STATE_PLAYING:
    state = "FLU_PLAYER_STATE_PLAYING";
    break;

  default:
    state = "FLU_PLAYER_STATE_STOPPED";
    break;
  }

  g_print("Info: player state %s.\n", state);
  return TRUE;
}

static gboolean on_error(FluPlayer *player, FluPlayerEvent *event, gpointer user_data)
{
  FluPlayerEventError *ev = (FluPlayerEventError *)event;
  Application *app = (Application *)user_data;

  g_print("Error: %s <%s>.\n", ev->error->message, ev->dbg);
  flu_player_dot_graph_generate(player, "player_error");

  application_exit(app);
  return FALSE;
}

static gboolean on_eos(FluPlayer *player, FluPlayerEvent *event, gpointer user_data)
{
  Application *app = (Application *)user_data;

  g_print("Info: EOS received.\n");

  application_exit(app);
  return FALSE;
}

static gboolean on_request_texture(FluPlayer *player, FluPlayerEvent *event, gpointer user_data)
{
  FluPlayerEventRequestTexture *ev = (FluPlayerEventRequestTexture *)event;
  CustomSource *source = (CustomSource *)user_data;

  if (flu_stream_get_source(ev->stream) != source->flu_source)
  {
    return TRUE;
  }

  g_print("Info: request texture for source %s.\n", flu_source_get_name(source->flu_source));

  return display_create(ev, source);
}

static gboolean on_need_data_source(FluPlayer *player, FluPlayerEvent *event, gpointer user_data)
{
  GstBuffer *buffer;
  FluPlayerEventSourceNeedData *ev = (FluPlayerEventSourceNeedData *)event;
  CustomSource *source = (CustomSource *)user_data;

  if (ev->source != source->flu_source)
  {
    return TRUE;
  }

  g_signal_emit_by_name(source->demuxer_sink, "pull-buffer", &buffer);

  if (buffer)
  {
    GstMapInfo mi;
    gst_buffer_map(buffer, &mi, GST_MAP_READ);

    ev->pts = GST_BUFFER_TIMESTAMP(buffer);
    ev->size = mi.size;
    ev->data = g_malloc(ev->size);
    memcpy(ev->data, mi.data, ev->size);
    ev->free_func = g_free;

    gst_buffer_unmap(buffer, &mi);
    return TRUE;
  }
  else
  {
    if (!flu_source_custom_eos_send(source->flu_source))
    {
      GST_WARNING("Failed to send EOS");
      on_eos(player, NULL, source->app);
    }

    return FALSE;
  }
}

static gboolean on_seek_source(FluPlayer *player, FluPlayerEvent *event, gpointer user_data)
{
  FluPlayerEventSeekSource *ev = (FluPlayerEventSeekSource *)event;
  CustomSource *source = (CustomSource *)user_data;

  if (ev->source != source->flu_source)
  {
    return TRUE;
  }

  switch (ev->format)
  {
  case FLU_FORMAT_BYTES:
    g_print("Info: source %s with type FLU_FORMAT_BYTES seeking to byte-offset: %" G_GINT64_FORMAT ".\n",
            flu_source_get_name(ev->source), ev->position);
    break;

  case FLU_FORMAT_TIME:
    g_print("Info: source %s with type FLU_FORMAT_TIME seeking to position: %" GST_TIME_FORMAT ".\n",
            flu_source_get_name(ev->source), GST_TIME_ARGS(ev->position));
    break;

  default:
    g_print("Error: source %s received an invalid seek format.\n",
            flu_source_get_name(ev->source));
    break;
  }

  demuxer_seek(source, ev->format, ev->position);

  return TRUE;
}

gboolean player_create_custom_source(Application *app, GstElement *demuxer_sink, CustomSourceType source_type)
{
  CustomSource *source;

  if (!app->flu_player)
  {
    app->flu_player = flu_player_new();
    flu_player_event_listener_add(app->flu_player, FLU_PLAYER_EVENT_STATE, on_state, NULL);
    flu_player_event_listener_add(app->flu_player, FLU_PLAYER_EVENT_ERROR, on_error, app);
    flu_player_event_listener_add(app->flu_player, FLU_PLAYER_EVENT_EOS, on_eos, app);
  }

  source = g_slice_new0(CustomSource);
  source->app = app;
  source->type = source_type;
  switch (source_type)
  {
  case VIDEO_SOURCE:
    source->flu_source = flu_source_custom_new("Video", FLU_FORMAT_TIME, FLU_SIZE_UNKNOWN, TRUE);
    flu_player_event_listener_add(app->flu_player, FLU_PLAYER_EVENT_REQUEST_TEXTURE, on_request_texture, source);
    break;

  case AUDIO_SOURCE:
    source->flu_source = flu_source_custom_new("Audio", FLU_FORMAT_TIME, FLU_SIZE_UNKNOWN, TRUE);
    break;

  default:
    g_slice_free(CustomSource, source);
    return FALSE;
  }
  source->demuxer_sink = gst_object_ref(demuxer_sink);

  flu_player_event_listener_add(app->flu_player, FLU_PLAYER_EVENT_NEED_DATA_SOURCE, on_need_data_source, source);
  flu_player_event_listener_add(app->flu_player, FLU_PLAYER_EVENT_SEEK_SOURCE, on_seek_source, source);

  flu_player_source_add(app->flu_player, flu_source_ref(source->flu_source));
  app->custom_sources = g_list_append(app->custom_sources, source);

  return TRUE;
}

static void destroy_custom_sources(Application *app)
{
  if (app->custom_sources)
  {
    GList *item;

    for (item = app->custom_sources; item; item = item->next)
    {
      CustomSource *source = item->data;
      display_close(source);
      flu_source_unref(source->flu_source);
      gst_object_unref(source->demuxer_sink);
      g_slice_free(CustomSource, source);
    }

    g_list_free(app->custom_sources);
    app->custom_sources = NULL;
  }
}

void player_destroy(Application *app)
{
  if (app->flu_player)
  {
    flu_player_close(app->flu_player);
    destroy_custom_sources(app);
    flu_player_unref(app->flu_player);
    app->flu_player = NULL;
  }
}

void player_toggle_state(Application *app)
{
  if (app->flu_player)
  {
    g_print("Info: toggling player state.\n");

    switch (flu_player_state_get(app->flu_player))
    {
    case FLU_PLAYER_STATE_PAUSED:
    case FLU_PLAYER_STATE_STOPPED:
      flu_player_play(app->flu_player);
      break;

    case FLU_PLAYER_STATE_PLAYING:
      flu_player_pause(app->flu_player);
      break;

    default:
      break;
    }
  }
}

void player_seek(Application *app, gint time_offset)
{
  if (app->flu_player)
  {
    gint64 position;
    g_print("Info: seeking %d seconds from now.\n", time_offset);

    if (!flu_player_position_get(app->flu_player, &position))
    {
      g_print("Error: cannot get current player position.\n");
      return;
    }

    g_print("Info: current player position is %" GST_TIME_FORMAT ".\n", GST_TIME_ARGS(position));
    position = MAX(position + time_offset * GST_SECOND, 0);
    g_print("Info: seeking to %" GST_TIME_FORMAT ".\n", GST_TIME_ARGS(position));

    if (!flu_player_position_set(app->flu_player, position, FALSE))
    {
      g_print("Error: cannot set player position.\n");
    }
  }
}

void player_print_position(Application *app)
{
  if (app->flu_player)
  {
    gint64 position;

    if (flu_player_position_get(app->flu_player, &position))
    {
      g_print("Info: current player position is %" GST_TIME_FORMAT ".\n", GST_TIME_ARGS(position));
    }
    else
    {
      g_print("Error: cannot get current player position.\n");
    }
  }
}

void player_volume_set(Application *app, gint volume)
{
  if (app->flu_player)
  {
    GList* item;
    gfloat fvol = CLAMP((gfloat)volume / 100.f, 0.f, 1.f);
    GList* streams = flu_player_audio_active_streams_get(app->flu_player);

    for (item = streams; item; item = item->next)
    {
      FluStream *stream = item->data;
      flu_stream_volume_set(stream, fvol);
    }

    flu_stream_list_free(streams);
  }
}

void player_step(Application *app, guint64 amount)
{
  if (app->flu_player)
  {
    if (!flu_player_step(app->flu_player, amount))
    {
      g_print("Error: player cannot steps by %" G_GUINT64_FORMAT " buffers.\n", amount);
    }
  }
}
